export default {
    id: 'movie1',
    title: 'Start Up',
    year: 2020,
    numberOfSeasons: 1,

    plot: 'Start-Up menceritakan kisah orang-orang di dunia perusahaan startup. Seo Dal Mi (Bae Suzy), karakter utama film ini bermimpi menjadi Steve Jobs dari Korea. Dia seorang petualang yang tidak memiliki banyak harta, tetapi memiliki rencana besar untuk dirinya sendiri.',
    cast: 'Bae Suzy, Nam Joo-hyuk, Kim Seon-ho, Kang Han-na',
    creator: 'Oh Choong-hwan',
    
    seasons: {
        items: [{
            id: 'season1',
            name: 'Season 1',
            episodes: {
                items: [{
                    id: 'episode1',
                    title: '1. Start Up',
                    poster: 'https://www.biem.co/wp-content/uploads/2020/10/start-up-ep-1.jpg',
                    duration: '1h 21m',
                    plot: 'Drakor ini bercerita tentang awal kisah kehidupan Seo Dal Mi (Bae Suzy) dan Won In Jae(Kang Han Na) di dunia bisnis. Sebenarnya, mereka adalah kakak beradik yang terpisah karena orang tuanya bercerai.',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }, {
                    id: 'episode2',
                    title: '2. Start Up',
                    poster: 'https://www.tentangsinopsis.com/wp-content/uploads/2019/11/Start-Up.jpg',
                    duration: '43m',
                    plot: 'Drakor ini bercerita tentang awal kisah kehidupan Seo Dal Mi (Bae Suzy) dan Won In Jae(Kang Han Na) di dunia bisnis. Sebenarnya, mereka adalah kakak beradik yang terpisah karena orang tuanya bercerai.',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }]
            }
        }, {
            id: 'season2',
            name: 'Season 2',
            episodes: {
                items: [{
                    id: 'episode3',
                    title: '1. New season',
                    poster: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/netflix/ep3.jpg',
                    duration: '41m',
                    plot: 'When Harvey\'s promotion requires him to recruit and hire a graduate of Harvard Law, he chooses Mike Ross. But Mike doesn\'t actualy have a law degree',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }, {
                    id: 'episode4',
                    title: '2. Are you subscribed?',
                    poster: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/netflix/ep0.jpg',
                    duration: '49m',
                    plot: 'An open-and-shut case becomes anything but when Harvey is accused of an inappropriate dalliance with a married woman.',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }]
            }
        }, {
            id: 'season3',
            name: 'Season 3',
            episodes: {
                items: [{
                    id: 'episode3',
                    title: '1. New season',
                    poster: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/netflix/ep3.jpg',
                    duration: '41m',
                    plot: 'When Harvey\'s promotion requires him to recruit and hire a graduate of Harvard Law, he chooses Mike Ross. But Mike doesn\'t actualy have a law degree',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }, {
                    id: 'episode4',
                    title: '2. Are you subscribed?',
                    poster: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/netflix/ep0.jpg',
                    duration: '49m',
                    plot: 'An open-and-shut case becomes anything but when Harvey is accused of an inappropriate dalliance with a married woman.',
                    video: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }]
            }
        }]
    }
}