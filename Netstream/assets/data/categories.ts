export default {
    items: [{
        id: 'category1',
        title: 'Popular on Netstream',
        movies: [{
            id: 'movie1',
            poster: 'https://www.dreamers.id/img_editor/39989/images/start-up-poster-drama-1.jpg',
        }, {
            id: 'movie2',
            poster: 'https://cdn.europosters.eu/image/750/posters/attack-on-titan-shingeki-no-kyojin-key-art-i22808.jpg',
        }, {
            id: 'movie3',
            poster: 'https://m.media-amazon.com/images/M/MV5BYTE3ODhjNzUtZjk1OS00YzcwLWFjYmQtMjVkZTI0ZTQxNDJhXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_.jpg',
        }, {
            id: 'movie4',
            poster: 'https://i.pinimg.com/originals/6a/f3/87/6af387457739795e0b206aa27b17b457.jpg',
        }, {
            id: 'movie5',
            poster: 'https://i.pinimg.com/736x/00/5c/57/005c570b158c5e45fa5601dace1b394d.jpg',
        }]
    }, {
        id: 'category2',
        title: 'Trending',
        movies: [{
            id: 'movie1',
            poster: 'https://www.dreamers.id/img_editor/39989/images/start-up-poster-drama-1.jpg',
        }, {
            id: 'movie2',
            poster: 'https://cdn.europosters.eu/image/750/posters/attack-on-titan-shingeki-no-kyojin-key-art-i22808.jpg',
        }, {
            id: 'movie3',
            poster: 'https://m.media-amazon.com/images/M/MV5BYTE3ODhjNzUtZjk1OS00YzcwLWFjYmQtMjVkZTI0ZTQxNDJhXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_.jpg',
        }, {
            id: 'movie4',
            poster: 'https://i.pinimg.com/originals/6a/f3/87/6af387457739795e0b206aa27b17b457.jpg',
        }, {
            id: 'movie5',
            poster: 'https://i.pinimg.com/736x/00/5c/57/005c570b158c5e45fa5601dace1b394d.jpg',
        }]
    }, {
        id: 'category3',
        title: 'Recommended For You',
        movies: [{
            id: 'movie1',
            poster: 'https://www.dreamers.id/img_editor/39989/images/start-up-poster-drama-1.jpg',
        }, {
            id: 'movie2',
            poster: 'https://cdn.europosters.eu/image/750/posters/attack-on-titan-shingeki-no-kyojin-key-art-i22808.jpg',
        }, {
            id: 'movie3',
            poster: 'https://m.media-amazon.com/images/M/MV5BYTE3ODhjNzUtZjk1OS00YzcwLWFjYmQtMjVkZTI0ZTQxNDJhXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_.jpg',
        }, {
            id: 'movie4',
            poster: 'https://i.pinimg.com/originals/6a/f3/87/6af387457739795e0b206aa27b17b457.jpg',
        }, {
            id: 'movie5',
            poster: 'https://i.pinimg.com/736x/00/5c/57/005c570b158c5e45fa5601dace1b394d.jpg',
        }]
    }]
}