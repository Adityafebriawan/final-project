import React, { useRef, useState } from 'react';
import { View, Text } from 'react-native';
import { Video } from 'expo-av';
import { Episode } from '../../types';
import { Playback } from 'expo-av/build/AV';
import styles from './styles';

interface VideoPlayerProps {
    episode: Episode;
}

const VideoPlayer = (props: VideoPlayerProps) => {
    const { episode } = props;

    const [status, setStatus] = useState({});
    const video = useRef(null);
    
    return (
        <Video
            ref={video}
            style={styles.video}
            source={{
                uri: episode.video,
            }}
            // posterSource={{
            //     uri: episode.poster,
            // }}

            // usePoster={true}
            useNativeControls
            resizeMode="contain"
            onPlaybackStatusUpdate={status => setStatus(() => status)}
        />
    )
}

export default VideoPlayer;