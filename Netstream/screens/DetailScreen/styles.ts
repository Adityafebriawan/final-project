import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: '30%',
        resizeMode: 'cover',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold'

    },
    match: {
        color: '#59d467',
        fontWeight: 'bold',
        marginRight: 5

    },
    year: {
        color: '#757575',
        marginRight: 5,

    },
    ageContainer: {
        backgroundColor: '#e6e229',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        paddingHorizontal: 3,
        marginRight: 5,

    },
    age: {

    },

    //Button
    playButton: {
        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 3,
        marginVertical: 5,
    },
    playButtonText: {
        Color: 'black',
        fontSize: 16,
        fontWeight: 'bold',
    },
    downloadButton: {
        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 3,
        marginVertical: 5,
    },
    downloadButtonText: {
        Color: 'black',
        fontSize: 16,
        fontWeight: 'bold',
    },
})

export default styles;